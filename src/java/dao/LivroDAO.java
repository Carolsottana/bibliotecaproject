/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Livro;

/**
 *
 * @author ca
 */
public class LivroDAO extends DAO<Livro> {
    private static final String allQuery =
                                "SELECT * FROM  biblio.livros;";
    
    private static final String readQuery =
                                "SELECT * " +
                                "FROM biblio.livros " +
                                "WHERE cod_livro = ?;";
    
    private static final String emprestimoAberto = 
                            "CREATE VIEW biblio.livros_livres AS " +
                            "SELECT cod_livro FROM biblio.livros " +
                            "EXCEPT " +
                            "SELECT cod_livro FROM biblio.emprestimo " +
                            "WHERE dt_devolucao IS NOT NULL; ";
    
    public LivroDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Livro t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Livro read(Integer id) throws SQLException {
        Livro livro = new Livro();
        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id);            
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {            
                    livro.setCodLivro(id);                
                    livro.setTitulo(result.getString("titulo"));
                    livro.setEdicao(result.getInt("edicao"));
                    livro.setNumSecao(result.getInt("num_secao"));
                    livro.setIdLivro(result.getString("id_livro"));
                    livro.setAutorUm(result.getString("autor_principal"));
                    livro.setAutorDois(result.getString("autor_secundário"));
                    livro.setData(result.getDate("ano"));
                } else {
                    throw new SQLException("Erro ao visualizar: livro não encontrado.");
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao visualizar: livro não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao visualizar livro.");
            }
        }
        return livro;
    }

    @Override
    public void update(Livro t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Livro> all() throws SQLException {
        List<Livro> livros = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(allQuery);
        ResultSet result = statement.executeQuery();) {
            while (result.next()) {                
                Livro livro = new Livro();
                
                livro.setCodLivro(result.getInt("cod_livro"));                
                livro.setTitulo(result.getString("titulo"));
                livro.setEdicao(result.getInt("edicao"));
                livro.setNumSecao(result.getInt("num_secao"));
                livro.setIdLivro(result.getString("id_livro"));
                livro.setAutorUm(result.getString("autor_principal"));
                livro.setAutorDois(result.getString("autor_secundário"));
                livro.setData(result.getDate("ano"));
                
                livros.add(livro);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException("Erro ao listar livros existentes.");
        }
        return livros;
    }        
    
}
