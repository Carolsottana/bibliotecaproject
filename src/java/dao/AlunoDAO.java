/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.math.BigInteger;
import java.util.List;
import model.Aluno;

/**
 *
 * @author ca
 */
public class AlunoDAO extends DAO<Aluno> {
     private static final String allQuery =
                                "SELECT * FROM  biblio.aluno al INNER JOIN biblio.pessoa pe "+
                                "ON al.aluno_cpf = pe.cpf;";

    public AlunoDAO(Connection connection) {
        super(connection);
    }
    
     @Override
    public List<Aluno> all() throws SQLException {
        List<Aluno> alunos = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
        ResultSet result = statement.executeQuery();) {
            while (result.next()) {                
                Aluno aluno = new Aluno();
                
                aluno.setCpfAluno(BigInteger.valueOf(result.getInt("aluno_cpf")));
                aluno.setCurso(result.getString("curso"));
                aluno.setMatricula(result.getInt("matricula"));
                aluno.setSemestre(result.getInt("semestre"));
                aluno.setNome(result.getString("pnome"));
                aluno.setSobrenome(result.getString("snome"));
                
                alunos.add(aluno);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException("Erro ao listar alunos existentes.");
        }
        return alunos;
    }        

    @Override
    public void create(Aluno t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public Aluno read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public void update(Aluno t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

}
