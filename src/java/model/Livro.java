/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ca
 */

import java.sql.Date;

public class Livro {
    private Integer codLivro;
    private float numSecao;
    private Integer edicao;
    private String idLivro;
    private String autorUm;
    private String autorDois;
    private String titulo;
    private Date data;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    private Date ano;

    public Integer getCodLivro() {
        return codLivro;
    }

    public void setCodLivro(Integer codLivro) {
        this.codLivro = codLivro;
    }

    public float getNumSecao() {
        return numSecao;
    }

    public void setNumSecao(float numSecao) {
        this.numSecao = numSecao;
    }

    public Integer getEdicao() {
        return edicao;
    }

    public void setEdicao(Integer edicao) {
        this.edicao = edicao;
    }

    public String getIdLivro() {
        return idLivro;
    }

    public void setIdLivro(String idLivro) {
        this.idLivro = idLivro;
    }

    public String getAutorUm() {
        return autorUm;
    }

    public void setAutorUm(String autorUm) {
        this.autorUm = autorUm;
    }

    public String getAutorDois() {
        return autorDois;
    }

    public void setAutorDois(String autorDois) {
        this.autorDois = autorDois;
    }

    public Date getAno() {
        return ano;
    }

    public void setAno(Date ano) {
        this.ano = ano;
    }
    
}
