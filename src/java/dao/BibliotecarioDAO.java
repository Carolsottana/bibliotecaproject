/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Usuarioemplates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Bibliotecario;


public class BibliotecarioDAO extends DAO<Bibliotecario> {

    private static final String createQuery =
                                "INSERT INTO biblio.bibliotecario(bibliotecario_cpf, crb) " +
                                "VALUES(?, ?);";

    private static final String readQuery =
                                "SELECT * FROM  biblio.bibliotecario bi INNER JOIN biblio.pessoa pe " +
                                "ON bi.bibliotecario_cpf = pe.cpf " +
                                "WHERE bi.crb = ?;";

    private static final String updateQuery =
                                "UPDATE biblio.bibliotecario " +
                                "SET crb = ?" +
                                "WHERE bibliotecario_cpf = ?;";

    private static final String deleteQuery =
                                "DELETE FROM biblio.bibliotecario  " +
                                "WHERE bibliotecario_cpf = ?;";
    
    private static final String allQuery =
                                "SELECT * FROM biblio.bibliotecario bi INNER JOIN biblio.pessoa pe " +
                                "ON bi.bibliotecario_cpf = pe.cpf;"; 

    public BibliotecarioDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Bibliotecario bibliotecario) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {            

            statement.setInt(1, bibliotecario.getCpf());
            statement.setInt(2, bibliotecario.getCrb());            
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uq_bibliotecario_cpf")) {
                throw new SQLException("Erro ao inserir bibliotecário: cpf já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao inserir bibliotecario: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao inserir bibliotecario.");
            }
        }
    }

    @Override
    public Bibliotecario read(Integer id) throws SQLException {
        Bibliotecario bibliotecario = new Bibliotecario();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    bibliotecario.setCrb(id);
                    bibliotecario.setCpf(result.getInt("bibliotecario_cpf"));                    
                    bibliotecario.setNome(result.getString("pnome"));             
                    bibliotecario.setSobrenome(result.getString("snome"));             
                } else {
                    throw new SQLException("Erro ao visualizar: bibliotecario não encontrado.");
                }
            }
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
            if (ex.getMessage().equals("Erro ao visualizar: usuário não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao visualizar usuário.");
            }
        }
        return bibliotecario;
    }


    @Override
    public void update(Bibliotecario bibliotecario) throws SQLException {
        String query;

        query = updateQuery;
        
        try (PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setInt(1, bibliotecario.getCrb());
            statement.setInt(2, bibliotecario.getCpf());
            

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao editar: bibliotecario não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao editar: bibliotecario não encontrado.")) {
                throw ex;
            } else if (ex.getMessage().contains("uq_bibliotecario_crb")) {
                throw new SQLException("Erro ao editar bibliotecario: crb já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao editar bibliotecario: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao editar bibliotecario.");
            }
        }
    }

    @Override
    public void delete(Integer cpf) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setInt(1, cpf);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao excluir: bibliotecario não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao excluir: bibliotecario não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao excluir bibliotecario.");
            }
        }

    }

    @Override
    public List<Bibliotecario> all() throws SQLException {
        List<Bibliotecario> bibliotecarioList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
             ResultSet result = statement.executeQuery();) {
            while (result.next()) {                
                Bibliotecario bibliotecario = new Bibliotecario();
                bibliotecario.setCpf(result.getInt("bibliotecario_cpf"));
                bibliotecario.setCrb(result.getInt("crb"));
                bibliotecario.setNome(result.getString("pnome"));
                bibliotecario.setSobrenome(result.getString("snome"));

                bibliotecarioList.add(bibliotecario);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar bibliotecarios presentes.");
        }

        return bibliotecarioList;
    }        
    
    
}