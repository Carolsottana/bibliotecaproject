Biblioteca - DC/UEL
======================

Trabalho de banco de Dados.

- **Instituição:** Universidade Estadual de Londrina
- **Curso:** Ciência da Computação
- **Disciplina:** Bancos de Dados
- **Professor:** Daniel S. Kaster
- **Aluna:** Ana Carolina S. de Pádua


Ambiente de desenvolvimento
===========================

- **Plataforma:** Java EE 7
- **IDE:** NetBeans 8
- **SGBD:** PostgreSQL 9.3
- **Sistema operacional:** Ubuntu 14.04 LTS



Bibliotecas utilizadas
======================

- [JSTL][1]
- [JDBC][2]
- [jQuery][3]
- [Bootstrap][4]
- [Gson][5]

[1]: https://jstl.java.net/
[2]: http://jdbc.postgresql.org/
[3]: http://jquery.com/
[4]: http://getbootstrap.com/
[5]: https://code.google.com/p/google-gson/


Para executar o projeto
=======================

Definir as seguintes propriedades no arquivo `src/java/jdbc/datasource.properties`:

- **host:** endereço IP da máquina em que o serviço do PostgreSQL está executando
- **port:** porta em que o serviço do PostgreSQL está executando
- **name:** nome do banco de dados
- **user:** usuário de conexão ao banco de dados
- **password:** senha de conexão ao banco de dados

Banco de dados:

```sql
CREATE SCHEMA biblio;

CREATE TABLE biblio.pessoa(
	cpf INT,
	data_nascimento DATE,
	pnome VARCHAR(40) NOT NULL,
	snome VARCHAR(40) NOT NULL,
	sexo CHAR(1),
	CONSTRAINT pk_pessoa PRIMARY KEY (cpf),
	CONSTRAINT un_pessoa UNIQUE (pnome, snome),
	CONSTRAINT ck_pessoa CHECK (sexo='F' OR sexo='M')
);

CREATE TABLE biblio.funcionario(
	func_cpf INT,
	salario NUMERIC(10,2),
	setor VARCHAR(40),
	CONSTRAINT pk_func PRIMARY KEY (func_cpf),
	CONSTRAINT fk_func FOREIGN KEY(func_cpf) REFERENCES biblio.pessoa(cpf) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE biblio.aluno(
	aluno_cpf INT,
	curso VARCHAR(40),
	matricula BIGINT NOT NULL,
	semestre INT,
	CONSTRAINT pk_aluno PRIMARY KEY (aluno_cpf),
	CONSTRAINT un_aluno UNIQUE(matricula),
	CONSTRAINT fk_aluno FOREIGN KEY(aluno_cpf) REFERENCES biblio.pessoa(cpf) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE biblio.bibliotecario(
	bibliotecario_cpf INT,
	crb BIGINT NOT NULL,
	CONSTRAINT pk_bibliotecario PRIMARY KEY (bibliotecario_cpf),
	CONSTRAINT fk_bibliotecario FOREIGN KEY (bibliotecario_cpf) REFERENCES biblio.funcionario(func_cpf),
	CONSTRAINT un_bibliotecario UNIQUE(crb)
);

CREATE TABLE biblio.secao(
	cod_secao SERIAL,
	num_secao FLOAT,
	bibliotecario_cpf INT NOT NULL DEFAULT 32,
	nome VARCHAR(60),
	CONSTRAINT pk_secao PRIMARY KEY (cod_secao) DEFERRABLE INITIALLY IMMEDIATE,
	CONSTRAINT un_secao UNIQUE(num_secao),
	CONSTRAINT fk_secao FOREIGN KEY(bibliotecario_cpf) REFERENCES biblio.bibliotecario(bibliotecario_cpf) ON DELETE SET DEFAULT
);

CREATE TABLE biblio.livros(
	cod_livro SERIAL,
	titulo VARCHAR(100),
	num_secao FLOAT,
	edicao INT,
	id_livro VARCHAR(10) NOT NULL,
	autor_principal VARCHAR(30) NOT NULL,
	autor_secundário VARCHAR(30),
	ano DATE,
	CONSTRAINT pk_livros PRIMARY KEY (cod_livro),
	CONSTRAINT fk_livros FOREIGN KEY(num_secao) REFERENCES biblio.secao(num_secao) INITIALLY DEFERRED
);

CREATE TABLE biblio.emprestimo(
	cod_emprestimo SERIAL,
	cod_livro INT,
	aluno_cpf INT,
	bibliotecario_cpf INT DEFAULT 32,
	dt_entrega DATE,
	dt_devolucao DATE,
	dt_emprestimo DATE DEFAULT 'today',
	CONSTRAINT pk_emprestimo PRIMARY KEY (cod_emprestimo),
	CONSTRAINT fk_emprestimo FOREIGN KEY(cod_livro) REFERENCES biblio.livros(cod_livro),
	CONSTRAINT fk_emprestimo_bibliotecario FOREIGN KEY(bibliotecario_cpf) REFERENCES biblio.bibliotecario(bibliotecario_cpf) ON DELETE SET DEFAULT,
	CONSTRAINT fk_emprestimo_aluno FOREIGN KEY(aluno_cpf) REFERENCES biblio.aluno(aluno_cpf),
	CONSTRAINT ck_dt_emprestimo CHECK(dt_emprestimo >= 'today'),
	CONSTRAINT ck_dt_entrega CHECK(dt_entrega >= 'today'),
	CONSTRAINT ck_dt_devolucao CHECK(dt_devolucao >= 'today'),
	CONSTRAINT un_livro_emp UNIQUE (cod_livro)
);

CREATE TABLE biblio.multa(
	cod_multa SERIAL,
	cod_emprestimo SERIAL,
	valor REAL,
	CONSTRAINT pk_multa PRIMARY KEY (cod_multa, cod_emprestimo),
	CONSTRAINT fk_multa FOREIGN KEY(cod_emprestimo) REFERENCES biblio.emprestimo(cod_emprestimo)  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE biblio.reservas(
	cod_reserva SERIAL,
	cod_livro SERIAL,
	aluno_cpf INT ,
	dt_reserva DATE DEFAULT 'today',
	dt_devolucao DATE,
	CONSTRAINT pk_reservas PRIMARY KEY (cod_reserva, cod_livro, aluno_cpf),
	CONSTRAINT fk_reservas_livro FOREIGN KEY(cod_livro) REFERENCES biblio.livros(cod_livro),
	CONSTRAINT fk_multa FOREIGN KEY(aluno_cpf) REFERENCES biblio.aluno(aluno_cpf)
);

CREATE TABLE biblio.aluno_pesquisa_livros(
	cod_livro SERIAL,
	aluno_cpf INT,
	CONSTRAINT pk_pesquisa_livros PRIMARY KEY (cod_livro, aluno_cpf),
	CONSTRAINT fk_pesquisa_livros FOREIGN KEY(cod_livro) REFERENCES biblio.livros(cod_livro),
	CONSTRAINT fk_pesquisa_alunos FOREIGN KEY(aluno_cpf) REFERENCES biblio.aluno(aluno_cpf)
);

CREATE TABLE biblio.aluno_reserva_livros(
	cod_livro SERIAL,
	aluno_cpf INT,
	CONSTRAINT pk_reserva_livros PRIMARY KEY (cod_livro, aluno_cpf),
	CONSTRAINT fk_reserva_livros FOREIGN KEY(cod_livro) REFERENCES biblio.livros(cod_livro),
	CONSTRAINT fk_reserva_alunos FOREIGN KEY(aluno_cpf) REFERENCES biblio.aluno(aluno_cpf)

);

CREATE TABLE biblio.bibliotecario_aluno_empresta_livro(
	cod_livro SERIAL,
	aluno_cpf INT,
	bibliotecario_cpf INT DEFAULT 32,
	CONSTRAINT pk_reserva_livro PRIMARY KEY (cod_livro, aluno_cpf, bibliotecario_cpf),
	CONSTRAINT fk_reserva_livros FOREIGN KEY(cod_livro) REFERENCES biblio.livros(cod_livro),
	CONSTRAINT fk_reserva_alunos FOREIGN KEY(aluno_cpf) REFERENCES biblio.aluno(aluno_cpf),
	CONSTRAINT fk_reserva_bibliotecario FOREIGN KEY(bibliotecario_cpf) REFERENCES biblio.bibliotecario(bibliotecario_cpf) ON DELETE SET DEFAULT
);

--INSERÇÃO DE DADOS NA TABELA;

-- Tabela de pessoa
-- Funcionario
INSERT INTO biblio.pessoa
VALUES (123, '2004-04-01', 'Ana', 'Padua', 'F');

INSERT INTO biblio.pessoa
VALUES (1234, '2004-04-01', 'Luiz', 'Roberto', 'M');

INSERT INTO biblio.pessoa
VALUES (54, '2005-05-02', 'Cassia', 'Sottana', 'F');

INSERT INTO biblio.pessoa
VALUES (54, '2005-05-02', 'Cassia', 'Sottana', 'F');

INSERT INTO biblio.pessoa
VALUES (32, '1993-05-10', 'Admin', 'Admin', 'F');

INSERT INTO biblio.pessoa
VALUES (0909, '1993-05-10', 'Teste', 'MeuTeste', 'M');

INSERT INTO biblio.pessoa
VALUES (11, '1993-05-10', 'Mar', 'Mar', 'F');

-- Aluno
INSERT INTO biblio.pessoa
VALUES (12, '2004-04-01', 'Ana', 'Sottana', 'F');

INSERT INTO biblio.pessoa
VALUES (12345, '2004-04-01', 'Rafael', 'Ito', 'M');

INSERT INTO biblio.pessoa
VALUES (5432, '2000-04-01', 'Daniel', 'Kaster', 'M');

INSERT INTO biblio.pessoa
VALUES (4301, '1993-04-01', 'Mariana', 'Castro', 'F');

-- Tabela de funcionários
INSERT INTO biblio.funcionario
VALUES (1234, 100.00, 'Bibliotecario');

INSERT INTO biblio.funcionario
VALUES (54, 200.00, 'Atendimento');

INSERT INTO biblio.funcionario
VALUES (123, 300.00, 'Bibliotecario');

INSERT INTO biblio.funcionario
VALUES (32, 500.00, 'Bibliotecario');

INSERT INTO biblio.funcionario
VALUES (0909, 500.00, 'Bibliotecario');

INSERT INTO biblio.funcionario
VALUES (11, 500.00, 'Bibliotecario');

-- Tabela de alunos
INSERT INTO biblio.aluno
VALUES (12, 'Computação', 201200560504, 5);

INSERT INTO biblio.aluno
VALUES (12345, 'Administração', 20150056240, 4);

INSERT INTO biblio.aluno
VALUES (5432, 'Administração', 201702025, 3);

INSERT INTO biblio.aluno
VALUES (4301, 'Quimica', 20150025, 1);



-- Tabela de bibliotecario
INSERT INTO biblio.bibliotecario
VALUES (1234, 2012089);

INSERT INTO biblio.bibliotecario
VALUES (123, 2015006);


-- Tabela de seção
INSERT INTO biblio.secao
VALUES (145, 546.090, 1234, 'Empreendedorismo');

INSERT INTO biblio.secao
VALUES (290, 541.870, 1234, 'Computação');

INSERT INTO biblio.secao
VALUES (782, 250.090, 123, 'Quimica');

INSERT INTO biblio.secao
VALUES (696, 950.000, 123, 'Engenharia Civil');


-- Tabela de livros
INSERT INTO biblio.livros
VALUES (145, 'A startup enxuta',  546.090, 2, 'H8MOP9', 'Eric Reis', NULL, '2011-07-20');

INSERT INTO biblio.livros
VALUES (110,'Banco de Dados', 541.870, 3, 'HS10D', 'Jose B. Alves', 'Navathe', '1995-08-22');

INSERT INTO biblio.livros
VALUES (146,'Os sete hábitos das pessoas altamente eficazes', 546.090, 1, 'MK10', 'Stephen Covey', NULL, '1989-01-10');

INSERT INTO biblio.livros
VALUES (111,'Introduction to automata theory ', 541.870, 3, 'J089M', 'Jose B. Alves', 'Navathe', '1995-08-22');

INSERT INTO biblio.livros
VALUES (405,'Concreto armado eu te amo', 950.000, 5, 'SO98I', 'Manoel Henrique', 'Jose Campos Botelho', '2000-04-02');

INSERT INTO biblio.livros
VALUES (406,'Calculo e detalhamento de estruturas', 950.000, 2, 'AS0P1', 'Roberto Carvalho', NULL, '20015-09-12');

INSERT INTO biblio.livros
VALUES (407,'Calculo e detalhamento de estruturas', 950.000, 5, 'AS0P1', 'Roberto Carvalho', NULL, '20015-09-12');

INSERT INTO biblio.livros
VALUES (305,'Quimica inorgânica', 250.090, 1, 'QMK010', 'Catherine E. Housecroft', 'Alan Sharpe', '2013-01-01');

INSERT INTO biblio.livros
VALUES (310,'Principios de Quimica', 250.090, 3, 'Q01MKA', 'Peter William Atkins', 'Julio de Paula', '2010-05-18');

-- Tabela de emprestimo
INSERT INTO biblio.emprestimo
VALUES (1, 145, 12, 1234, '2018-01-20', NULL, 'today');

INSERT INTO biblio.emprestimo
VALUES (2, 146, 12345, 1234, '2018-01-20', NULL, 'today');

INSERT INTO biblio.emprestimo
VALUES (3, 405, 12345, 1234, '2018-01-20', NULL, 'today');

INSERT INTO biblio.emprestimo
VALUES (4, 406, 12345, 1234, '2018-01-20', NULL, 'today');

INSERT INTO biblio.emprestimo
VALUES (5, 407, 12, 1234, '2018-01-20', NULL, 'today');

INSERT INTO biblio.emprestimo
VALUES (7, 310, 12345, 123, '2018-01-20', '2018-01-21', 'today');

-- Tabela de multa
INSERT INTO biblio.multa
VALUES (13, 1, 1);


-- Tabela de reservas
INSERT INTO biblio.reservas
VALUES(1,145,12,'2017-07-22', '2017-10-24');

INSERT INTO biblio.reservas
VALUES(2,146,12,'2017-07-23', '2017-10-24');

INSERT INTO biblio.reservas
VALUES(3,111,12,'2017-07-23', '2017-10-25');

INSERT INTO biblio.reservas
VALUES(4,111,12345,'2017-07-23', '2017-10-25');

INSERT INTO biblio.aluno_pesquisa_livros
VALUES (145,12);

INSERT INTO biblio.aluno_reserva_livros
VALUES (146,12);

INSERT INTO biblio.aluno_reserva_livros
VALUES (145,12);

INSERT INTO biblio.aluno_reserva_livros
VALUES (111,12);

INSERT INTO biblio.aluno_reserva_livros
VALUES (111,12345);

INSERT INTO biblio.bibliotecario_aluno_empresta_livro
VALUES (146, 12345, 1234);

INSERT INTO biblio.bibliotecario_aluno_empresta_livro
VALUES (405, 12345, 1234);

INSERT INTO biblio.bibliotecario_aluno_empresta_livro
VALUES (406, 12345, 1234);


```
