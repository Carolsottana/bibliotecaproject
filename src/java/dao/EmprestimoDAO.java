    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Emprestimo;

/**
 *
 * @author ca
 */
public class EmprestimoDAO extends DAO<Emprestimo>{
    
    private static final String createQuery =
                                "INSERT INTO biblio.emprestimo(cod_emprestimo, cod_livro, aluno_cpf, bibliotecario_cpf, dt_entrega) " +
                                "VALUES(?, ?, ?, ?, ?);";

    private static final String readQuery =
                                "SELECT * " +
                                "FROM biblio.emprestimo em " +
                                "WHERE em.cod_emprestimo = ?;";

    private static final String updateQuery =
                                "UPDATE biblio.emprestimo " +
                                "SET dt_devolucao = ? " +
                                "WHERE cod_emprestimo = ?;";

    private static final String deleteQuery =
                                "DELETE FROM biblio.emprestimo  " +
                                "WHERE cod_emprestimo = ?;";
    
    private static final String allQuery =
                                "SELECT * FROM  biblio.emprestimo; ";   
    
    private static final String allAberto =
                                "SELECT * FROM  biblio.historico_de_emprestimos "+
                                "WHERE dt_emprestimo BETWEEN ? AND ? ;";   
    
    private static final String atualizaMaterializa = 
                                "SELECT biblio.atualiza_materializada();";
    
    public EmprestimoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Emprestimo emprestimo) throws SQLException {
         try (PreparedStatement statement = connection.prepareStatement(createQuery);) {                        
            statement.setInt(1, emprestimo.getCodEmprestimo());
            statement.setInt(2, emprestimo.getCodLivro());
            statement.setInt(3, emprestimo.getAlCpf());          
            statement.setInt(4, emprestimo.getBiCpf());
            statement.setDate(5, emprestimo.getDataEntrega());                        
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uq_emprestimo_cod_emprestimo")) {
                throw new SQLException("Erro ao realizar emprestimo: cod_emprestimo já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao realizar emprestimo: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao realizar emprestimo.");
            }
        }
    }

    @Override
    public Emprestimo read(Integer id) throws SQLException {
        Emprestimo emprestimo = new Emprestimo();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id);            
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    
                    emprestimo.setCodEmprestimo(id);
                    emprestimo.setCodLivro(result.getInt("cod_livro"));
                    emprestimo.setAlCpf(result.getInt("aluno_cpf"));
                    emprestimo.setBiCpf(result.getInt("bibliotecario_cpf"));
                    emprestimo.setDataEmprestimo(result.getDate("dt_emprestimo"));
                    emprestimo.setDataEntrega(result.getDate("dt_entrega"));
                    emprestimo.setDataDevolucao(result.getDate("dt_devolucao"));

                } else {
                    throw new SQLException("Erro ao visualizar: emprestimo não encontrado.");
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao visualizar: emprestimo não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao visualizar emprestimo.");
            }
        }
        return emprestimo;
    }

    @Override
    public void update(Emprestimo emprestimo) throws SQLException {
        String query;

        query = updateQuery;
        try (PreparedStatement statement = connection.prepareStatement(query);) {            
            statement.setDate(1, emprestimo.getDataDevolucao());            
            statement.setInt(2, emprestimo.getCodEmprestimo());                        
            
            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao editar: emprestimo não encontrado.");
            }
            //Statement staments = connection.createStatement();
            //staments.executeUpdate(atualizaMaterializa);
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao editar: emprestimo não encontrado.")) {
                throw ex;
            } else if (ex.getMessage().contains("uq_emprestimo_cod_emprestimo")) {
                throw new SQLException("Erro ao editar emprestimo: cod_emprestimo já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao editar emprestimo: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao editar emprestimo.");
            }
        }
    }

    @Override
    public void delete(Integer cod_emp) throws SQLException {
         try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setInt(1, cod_emp);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao excluir: emprestimo não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao excluir: emprestimo não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao excluir emprestimo.");
            }
        }
    }

    @Override
    public List<Emprestimo> all() throws SQLException {
        List<Emprestimo> emprestimoList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
             ResultSet result = statement.executeQuery();) {
            while (result.next()) {                
                Emprestimo emprestimo = new Emprestimo();                
                emprestimo.setCodEmprestimo(result.getInt("cod_emprestimo"));
                emprestimo.setCodLivro(result.getInt("cod_livro"));
                emprestimo.setAlCpf(result.getInt("aluno_cpf"));
                emprestimo.setBiCpf(result.getInt("bibliotecario_cpf"));
                emprestimo.setDataEmprestimo(result.getDate("dt_emprestimo"));
                emprestimo.setDataEntrega(result.getDate("dt_entrega"));
                emprestimo.setDataDevolucao(result.getDate("dt_devolucao"));
                
                emprestimoList.add(emprestimo);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar emprestimos existentes.");
        }

        return emprestimoList;
    }        
    
     public List<Emprestimo> allAberto(Date um, Date dois) throws SQLException {
        List<Emprestimo> abertoList = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(allAberto);) {            
            statement.setDate(1, um);            
            statement.setDate(2, dois);
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {                
                    Emprestimo emprestimo = new Emprestimo();                        
                    emprestimo.setCodLivro(result.getInt("cod_livro"));
                    emprestimo.setAlCpf(result.getInt("aluno_cpf"));
                    emprestimo.setDataEmprestimo(result.getDate("dt_emprestimo"));
                    emprestimo.setDataEntrega(result.getDate("dt_entrega"));
                    emprestimo.setDataDevolucao(result.getDate("dt_devolucao"));

                    abertoList.add(emprestimo);
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar historico de emprestimos.");
        }

        return abertoList;
    } 
    
}
