/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author ca
 */

public class Emprestimo {
    private Integer codEmprestimo;
    private Integer codLivro;
    private Integer alCpf;
    private Integer biCpf;
    private Date dataDevolucao;
    private Date dataEmprestimo;
    private Date dataEntrega;
    
    public Integer getCodEmprestimo() {
        return codEmprestimo;
    }

    public void setCodEmprestimo(Integer codEmprestimo) {
        this.codEmprestimo = codEmprestimo;
    }

    public Integer getCodLivro() {
        return codLivro;
    }

    public void setCodLivro(Integer codLivro) {
        this.codLivro = codLivro;
    }

    public Integer getAlCpf() {
        return alCpf;
    }

    public void setAlCpf(Integer alCpf) {
        this.alCpf = alCpf;
    }

    public Integer getBiCpf() {
        return biCpf;
    }

    public void setBiCpf(Integer biCpf) {
        this.biCpf = biCpf;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Date getDataEmprestimo() {
        return dataEmprestimo;
    }

    public void setDataEmprestimo(Date dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }
    
}
