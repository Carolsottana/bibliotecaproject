<%-- 
    Document   : welcome
    Created on : 26/10/2017, 15:12:02
    Author     : dskaster
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.servletContext.contextPath}/assets/css/navbar.css" rel="stylesheet">        
        <title>Início</title>
    </head>
    <body>
        <%@include file="/view/include/navbar.jsp"%>

        <div class="container">
            <div class="jumbotron">
                <h1>Bem-vindo a Biblioteca!</h1>
                <p>Sistema web implementado para a disciplina de Banco de Dados.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="https://bitbucket.org/Carolsottana/bibliotecaproject" target="_blank">
                        Repositório GIT do projeto da biblioteca
                    </a>
                </p>
            </div>
        </div>
                
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>
    </body>
</html>
