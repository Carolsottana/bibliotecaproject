/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAO;
import dao.DAOFactory;
import dao.EmprestimoDAO;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Emprestimo;
import model.Livro;
import model.Bibliotecario;
import model.Aluno;
/**
 *
 * @author ca
 */
@WebServlet(name = "EmprestimoController", urlPatterns = {"/emprestimo", "/emprestimo/create", "/emprestimo/read", "/emprestimo/delete", "/emprestimo/update", "/emprestimo/aberto", "/emprestimo/historico"})
public class EmprestimoController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAO dao, daoL, daoB;
        RequestDispatcher dispatcher;
        String[] dataUm;
        String[] dataDois;

        switch (request.getServletPath()) {
            case "/emprestimo":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getEmprestimoDAO();

                    List<Emprestimo> emprestimoList = dao.all();
                    request.setAttribute("emprestimoList", emprestimoList);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    //request.getSession().setAttribute("error", ex.getMessage());
                }

                dispatcher = request.getRequestDispatcher("/view/emprestimo/index.jsp");
                dispatcher.forward(request, response);
                break;            

            case "/emprestimo/create":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    daoL = daoFactory.getLivroDAO();
                    daoB = daoFactory.getBibliotecarioDAO();
                    dao = daoFactory.getAlunoDAO();
                
                    List<Livro> livroList = daoL.all();
                    List<Aluno> alunoList = dao.all();
                    List<Bibliotecario> bibliotecarioList = daoB.all();

                    request.setAttribute("livroList", livroList);
                    request.setAttribute("bibliotecarioList", bibliotecarioList);
                    request.setAttribute("alunoList", alunoList);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    request.getSession().setAttribute("error", ex.getMessage());
                }
                dispatcher = request.getRequestDispatcher("/view/emprestimo/create.jsp");
                dispatcher.forward(request, response);

                break;
            case "/emprestimo/aberto":                
                dispatcher = request.getRequestDispatcher("/view/emprestimo/aberto.jsp");
                dispatcher.forward(request, response);

                break;
                
                
            case "/emprestimo/read":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getEmprestimoDAO();

                    Emprestimo emprestimo = (Emprestimo) dao.read(Integer.parseInt(request.getParameter("codEmprestimo")));

                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //request.getSession().setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/emprestimo");
                }

                break;

            case "/emprestimo/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getEmprestimoDAO();

                    dao.delete(Integer.parseInt(request.getParameter("codEmprestimo")));
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    //request.getSession().setAttribute("error", ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/emprestimo");

                break;

            case "/emprestimo/update":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getEmprestimoDAO();

                    Emprestimo emprestimo = (Emprestimo) dao.read(Integer.parseInt(request.getParameter("codEmprestimo")));
                    request.setAttribute("emprestimo", emprestimo);

                    dispatcher = request.getRequestDispatcher("/view/emprestimo/update.jsp");
                    dispatcher.forward(request, response);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //request.getSession().setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/emprestimo");
                }
                break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAO dao;
        Emprestimo emprestimo = new Emprestimo();
        String[] entrega;
        String[] devolucao;
        HttpSession session = request.getSession();
        String[] dataUm;
        String[] dataDois;
        Date dataU;
        Date dataD; 
        RequestDispatcher dispatcher;

        switch (request.getServletPath()) {
            case "/emprestimo/create":

                try (DAOFactory daoFactory = new DAOFactory();) {
                    request.setCharacterEncoding("UTF-8");


                    emprestimo.setCodEmprestimo(Integer.parseInt(request.getParameter("codemp")));
                    emprestimo.setCodLivro(Integer.parseInt(request.getParameter("codLivro")));
                    emprestimo.setAlCpf(Integer.parseInt(request.getParameter("cpfAluno")));
                    emprestimo.setBiCpf(Integer.parseInt(request.getParameter("cpf")));


                    entrega = request.getParameter("entrega").split("/");
                    emprestimo.setDataEntrega(Date.valueOf(entrega[2] + "-" + entrega[1] + "-" + entrega[0]));


                    dao = daoFactory.getEmprestimoDAO();

                    dao.create(emprestimo);

                    response.sendRedirect(request.getContextPath() + "/emprestimo");                    
                
                } catch (ClassNotFoundException | IOException | SQLException | IllegalArgumentException ex) {
                    if (ex instanceof IllegalArgumentException) {
                        session.setAttribute("error", "Erro de formato de data.");
                    } else {
                        session.setAttribute("error", ex.getMessage());
                    }
                    response.sendRedirect(request.getContextPath() + "/emprestimo/create");
                }

                break;
                
            case "/emprestimo/historico":
                
                try (DAOFactory daoFactory = new DAOFactory();) {
                    request.setCharacterEncoding("UTF-8");

                    dataUm = request.getParameter("inicio").split("/");
                    dataU = (Date.valueOf(dataUm[2] + "-" + dataUm[1] + "-" + dataUm[0]));      
                    
                    dataDois = request.getParameter("final").split("/");
                    dataD = (Date.valueOf(dataDois[2] + "-" + dataDois[1] + "-" + dataDois[0])); 
                    
                    EmprestimoDAO daos;
                    daos = daoFactory.getEmprestimoDAO();                    
                                       
                    List<Emprestimo> abertoList = daos.allAberto(dataU, dataD);
                    request.setAttribute("abertoList", abertoList);
                    dispatcher = request.getRequestDispatcher("/view/emprestimo/historico.jsp");
                    dispatcher.forward(request, response);
                    
                } catch (ClassNotFoundException | IOException | SQLException | IllegalArgumentException ex) {
                    if (ex instanceof IllegalArgumentException) {
                        session.setAttribute("error", "Erro de formato de data.");
                    } else {
                        session.setAttribute("error", ex.getMessage());
                    }
                    response.sendRedirect(request.getContextPath() + "/emprestimo/aberto");
                }
                break;

            case "/emprestimo/update":
                request.setCharacterEncoding("UTF-8");
                    emprestimo.setCodEmprestimo(Integer.parseInt(request.getParameter("codemp")));

                    devolucao = request.getParameter("devolucao").split("/");
                    emprestimo.setDataDevolucao(Date.valueOf(devolucao[2] + "-" + devolucao[1] + "-" + devolucao[0]));

                try (DAOFactory daoFactory = new DAOFactory();) {                    
                    dao = daoFactory.getEmprestimoDAO();
                    dao.update(emprestimo);
                    
                    response.sendRedirect(request.getContextPath() + "/emprestimo");
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    session.setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/emprestimo/update?id=" + emprestimo.getCodEmprestimo());
                }

                break;

            case "/emprestimo/delete":
                String[] emprestimos = request.getParameterValues("delete");

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getEmprestimoDAO();

                    try {
                        daoFactory.beginTransaction();

                        for (int i = 0; i < emprestimos.length; ++i) {
                            dao.delete(Integer.parseInt(emprestimos[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        //session.setAttribute("error", ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //session.setAttribute("error", ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/emprestimo");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
