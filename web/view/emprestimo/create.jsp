<%-- 
    Document   : create
    Created on : 27/09/2017, 11:34:54
    Author     : dskaster
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/usuario_form.css" rel="stylesheet">        
        <title>Criação de Usuários</title>
    </head>
    <body>

        <div class="container">
            <h2 class="text-center">Realizar um emprestimo</h2>
            
            <form class="form-group" action="${pageContext.servletContext.contextPath}/emprestimo/create" method="POST">                 
                <label class="h4">Cod. Emprestimo</label>
                <input class="form-control" type="text" name="codemp" required>
                <span>Escolha o livro desejado</span>                
                <select class="form-control" name="codLivro" required>                        
                    <c:forEach var="l" items="${livroList}">  
                        <option value="${l.codLivro}" ><c:out value="${l.codLivro}"/> - <c:out value="${l.titulo}"/></option>                
                    </c:forEach>
                </select>   
                <span>Escolha o aluno a realizar o empréstimo</span>                
                <select class="form-control" name="cpfAluno" required>                        
                    <c:forEach var="f" items="${alunoList}">  
                        <option value="${f.cpfAluno}" >
                            <c:out value="${f.nome}"/> <c:out value="${f.sobrenome}"/>
                        </option>                
                    </c:forEach>
                </select>                                  
                <span>Escolha o bibliotecario a realizar o empréstimo</span>                
                <select class="form-control" name="cpf" required>                        
                    <c:forEach var="b" items="${bibliotecarioList}">  
                        <option value="${b.cpf}" >
                            <c:out value="${b.nome}"/> <c:out value="${b.sobrenome}"/>
                        </option>                
                    </c:forEach>
                </select>     

                <label class="h4">Data da entrega</label>
                <input class="form-control datepicker" type="text" name="entrega" placeholder="dd/mm/yyyy" pattern="\d{2}/\d{2}/\d{4}" required>
                <div class="text-center">
                    <button class="btn btn-lg btn-success btn-block" type="submit">Inserir</button>
                </div>
            </form>

        </div>
        
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.pt-BR.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/main.js"></script>                    
    </body>
</html>
