<%--
    Document   : index
    Created on : 26/09/2017, 15:43:54
    Author     : dskaster
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/usuario_index.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/navbar.css" rel="stylesheet">
        <title>Bibliotecarios</title>
    </head>
    <body>
        <%@include file="/view/include/navbar.jsp"%>
        <div class="container">
            <h2 class="text-center">Coloque o período ao qual deseja pesquisar</h2>
            <form class="form_excluir_usuarios" action="${pageContext.servletContext.contextPath}/emprestimo/historico" method="POST">              
                <div class="col-lg-6">
                    <label class="h4">Data Inicio</label>
                    <input class="form-control datepicker" type="text" name="inicio" placeholder="dd/mm/yyyy" pattern="\d{2}/\d{2}/\d{4}" required>
                </div>      
                <div class="col-lg-6">
                    <label class="h4">Data Final</label>
                    <input class="form-control datepicker" type="text" name="final" placeholder="dd/mm/yyyy" pattern="\d{2}/\d{2}/\d{4}" required>                
                </div>
                <div class="col-lg-12" style="padding-top: 2%;">
                    <div class="text-center">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Buscar</button>
                    </div>
                </div>
            </form>
        </div>

 
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.pt-BR.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/main.js"></script>
    </body>
</html>
