<%-- 
    Document   : update
    Created on : 24/10/2017, 14:56:31
    Author     : dskaster
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/datepicker.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/usuario_form.css" rel="stylesheet">
        <title>Atualização de bibliotecarios</title>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center">Edição do bibliotecario <c:out value="${bibliotecario.nome}"/></h2>

            <form class="form-group" action="${pageContext.servletContext.contextPath}/bibliotecario/update" method="POST">
                <label class="h4">CPF</label>
                <input class="form-control" type="text" name="cpf" value="${bibliotecario.cpf}" disabled>
                
                <label class="h4">CRB</label>
                <input class="form-control" type="text" name="crb" value="${bibliotecario.crb}" required autofocus>

                <input type="hidden" name="cpf" value="${bibliotecario.cpf}">

                <div class="text-center">
                    <a class="btn btn-lg btn-danger" href="${pageContext.servletContext.contextPath}/bibliotecario">
                        Cancelar
                    </a>
                    <button class="btn btn-lg  btn-success" type="submit">Editar</button>
                </div>
            </form>
        </div>

        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>        
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap-datepicker.pt-BR.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/main.js"></script>
    </body>
</html>
