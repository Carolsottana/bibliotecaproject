<%-- 
    Document   : index
    Created on : 26/09/2017, 15:43:54
    Author     : dskaster
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/usuario_index.css" rel="stylesheet">       
        <link href="${pageContext.servletContext.contextPath}/assets/css/navbar.css" rel="stylesheet">
        <title>Bibliotecarios</title>
    </head>
    <body>
        <%@include file="/view/include/navbar.jsp"%>
        <div class="container">
            <div class="text-center div_inserir_excluir col-lg-4 col-lg-offset-2">
                <a class="btn btn-lg btn-warning btn-block" href="${pageContext.servletContext.contextPath}/emprestimo/create">
                    Gerar um emprestimo
                </a>
            </div>
            <div class="text-center div_inserir_excluir col-lg-4">
                <a class="btn btn-lg btn-danger btn-block" href="${pageContext.servletContext.contextPath}/emprestimo/aberto">
                   Busca por Data
                </a>
            </div>         
            <form class="form_excluir_usuarios" action="${pageContext.servletContext.contextPath}/emprestimo/delete" method="POST">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="col-lg-5 h4">Cod. Livro</th>
                            <th class="col-lg-5 h4">Aluno</th>
                            <th class="col-lg-5 h4">Data do Emprestimo</th>
                            <th class="col-lg-5 h4">Data de Entrega</th>
                            <th class="col-lg-5 h4">Data de Devolução</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="u" items="${abertoList}">
                        <tr>
                                                      
                            <td>
                                <span class="h4"><c:out value="${u.codLivro}"/></span>
                            </td>           
                            <td>
                                <span class="h4"><c:out value="${u.alCpf}"/></span>
                            </td>              
                            <td>
                                <span class="h4"><c:out value="${u.dataEmprestimo}"/></span>
                            </td>     
                             <td>
                                <span class="h4"><c:out value="${u.dataEntrega}"/></span>
                            </td>  
                             <td>
                                <span class="h4"><c:out value="${u.dataDevolucao}"/></span>
                            </td>                              
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </form>
        </div>          

        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/main.js"></script>                    
    </body>
</html>
