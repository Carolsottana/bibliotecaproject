/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAO;
import dao.DAOFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Bibliotecario;
import model.Funcionario;
/**
 *
 * @author dskaster
 */
@WebServlet(name = "BibliotecarioController", urlPatterns = {"/bibliotecario", "/bibliotecario/create", "/bibliotecario/read", "/bibliotecario/delete", "/bibliotecario/update"})
public class BibliotecarioController extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAO dao;
        RequestDispatcher dispatcher;

        switch (request.getServletPath()) {
            case "/bibliotecario":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    List<Bibliotecario> bibliotecarioList = dao.all();
                    request.setAttribute("bibliotecarioList", bibliotecarioList);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    request.getSession().setAttribute("error", ex.getMessage());
                }

                dispatcher = request.getRequestDispatcher("/view/bibliotecario/index.jsp");
                dispatcher.forward(request, response);
                break;

            case "/bibliotecario/create":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getFuncionarioDAO();
                    List<Funcionario> funcionarioList = dao.all();
                    request.setAttribute("funcionarioList", funcionarioList);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    //request.getSession().setAttribute("error", ex.getMessage());
                }
                dispatcher = request.getRequestDispatcher("/view/bibliotecario/create.jsp");
                dispatcher.forward(request, response);
                break;

            case "/bibliotecario/read":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    Bibliotecario bibliotecario = (Bibliotecario) dao.read(Integer.parseInt(request.getParameter("cpf")));

                    Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
                    String json = gson.toJson(bibliotecario);

                    response.setContentType("application/json; charset=UTF-8");
                    response.getWriter().print(json);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    request.getSession().setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/bibliotecario");
                }

                break;

            case "/bibliotecario/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    dao.delete(Integer.parseInt(request.getParameter("cpf")));
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    System.out.println("Erro: "+ex.getMessage());
                    //request.getSession().setAttribute("error", ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/bibliotecario");

                break;

            case "/bibliotecario/update":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    Bibliotecario bibliotecario = (Bibliotecario) dao.read(Integer.parseInt(request.getParameter("crb")));
                    request.setAttribute("bibliotecario", bibliotecario);

                    dispatcher = request.getRequestDispatcher("/view/bibliotecario/update.jsp");
                    dispatcher.forward(request, response);
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //request.getSession().setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/bibliotecario");
                }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAO dao;
        Bibliotecario bibliotecario = new Bibliotecario();
        //HttpSession session = request.getSession();

        switch (request.getServletPath()) {
            case "/bibliotecario/create":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    request.setCharacterEncoding("UTF-8");
                    bibliotecario.setCrb(Integer.parseInt(request.getParameter("crb")));
                    bibliotecario.setCpf(Integer.parseInt(request.getParameter("cpf")));

                    dao = daoFactory.getBibliotecarioDAO();

                    dao.create(bibliotecario);

                    response.sendRedirect(request.getContextPath() + "/bibliotecario");

                } catch (ClassNotFoundException | IOException | SQLException | IllegalArgumentException ex) {
                    if (ex instanceof IllegalArgumentException) {
                        //session.setAttribute("error", "Erro de formato de data.");
                    } else {
                        //session.setAttribute("error", ex.getMessage());
                    }
                    response.sendRedirect(request.getContextPath() + "/bibliotecario/create");
                }

                break;

            case "/bibliotecario/update":
                request.setCharacterEncoding("UTF-8");
                bibliotecario.setCpf(Integer.parseInt(request.getParameter("cpf")));
                bibliotecario.setCrb(Integer.parseInt(request.getParameter("crb")));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    dao.update(bibliotecario);

                    response.sendRedirect(request.getContextPath() + "/bibliotecario");
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //session.setAttribute("error", ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/bibliotecario/update?id=" + bibliotecario.getCpf());
                }

                break;

            case "/bibliotecario/delete":
                String[] bibliotecarios = request.getParameterValues("delete");

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getBibliotecarioDAO();

                    try {
                        daoFactory.beginTransaction();

                        for (int i = 0; i < bibliotecarios.length; ++i) {
                            dao.delete(Integer.parseInt(bibliotecarios[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        //session.setAttribute("error", ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    //session.setAttribute("error", ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/bibliotecario");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
