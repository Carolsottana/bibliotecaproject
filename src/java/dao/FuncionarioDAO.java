/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Funcionario;

/**
 *
 * @author ca
 */
public class FuncionarioDAO extends DAO<Funcionario> {
     private static final String allQuery =
                                "SELECT * FROM  biblio.funcionario "+
                                "WHERE setor = 'Bibliotecario';";

    public FuncionarioDAO(Connection connection) {
        super(connection);
    }
    
     @Override
    public List<Funcionario> all() throws SQLException {
        List<Funcionario> funcionarios = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
        ResultSet result = statement.executeQuery();) {
            while (result.next()) {                
                Funcionario funcionario = new Funcionario();
                funcionario.setCpf(result.getInt("func_cpf"));
                funcionario.setSetor(result.getString("setor"));
                funcionario.setSalario(result.getFloat("salario"));
                
                funcionarios.add(funcionario);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException("Erro ao listar funcionarios existentes.");
        }
        return funcionarios;
    }        

    @Override
    public void create(Funcionario t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public Funcionario read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public void update(Funcionario t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //não implementado
    }

}
