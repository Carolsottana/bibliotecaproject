function showDatepicker() {
    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    }
}

function readBibliotecario() {
    $.getJSON($(this).data('href'), function (data) {
        var bibliotecario = data;
        $('.bCrf').html('<strong>Cpf: </strong>' + bibliotecario.cpf);
        $('.bCrb').html('<strong>Crb: </strong>' + bibliotecario.crb);
        $('.bNome').html('<strong>Nome: </strong>' + bibliotecario.nome);
        $('.bSobrenome').html('<strong>Sobrenome: </strong>' + bibliotecario.sobrenome);
        $('.modal_visualizar_usuario').modal();
    });
}

function readLivro() {
    $.getJSON($(this).data('href'), function (data) {
        var livro = data;
        $('.lTitulo').html('<strong>Titulo: </strong>' + livro.titulo);
        $('.lSecao').html('<strong>Secao: </strong>' + livro.numSecao);
        $('.lEdicao').html('<strong>Edicao: </strong>' + livro.edicao);
        $('.lAutorP').html('<strong>Autor Principal: </strong>' + livro.autorUm);
        $('.lAutors').html('<strong>Autor Secundário: </strong>' + livro.autorDois);
        $('.modal_visualizar_livro').modal();
    });
}

function deleteUser() {
    $('.link_confirmacao_excluir_usuario').attr('href', $(this).data('href'));
    $('.modal_excluir_usuario').modal();
}

function deleteEmp() {
    $('.link_confirmacao_excluir_emprestimo').attr('href', $(this).data('href'));
    $('.modal_excluir_emprestimo').modal();
}

function deleteUsers() {
    $('.form_excluir_usuarios').submit();
}

$(document).ready(function () {
    showDatepicker();
    //showErrors();
    $(document).on('click', '.link_excluir_emprestimo', deleteEmp);
    $(document).on('click', '.link_excluir_usuario', deleteUser);
    $(document).on('click', '.button_confirmacao_excluir_usuarios', deleteUsers);
    $(document).on('click', '.link_visualizar_usuario', readBibliotecario);
    $(document).on('click', '.link_visualizar_livro', readLivro);
});