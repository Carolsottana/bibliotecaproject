<%-- 
    Document   : create
    Created on : 27/09/2017, 11:34:54
    Author     : dskaster
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="my"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.servletContext.contextPath}/assets/css/usuario_form.css" rel="stylesheet">        
        <title>Criação de Usuários</title>
    </head>
    <body>

        <div class="container">
            <h2 class="text-center">Inserção de um novo bibliotecario</h2>
            
            <form class="form-group" action="${pageContext.servletContext.contextPath}/bibliotecario/create" method="POST">                 
                <span>Escolha um funcionario para criar o bibliotecario:</span>                
                <select class="form-control" name="cpf">                        
                    <c:forEach var="f" items="${funcionarioList}">  
                        <option value="${f.cpf}" ><c:out value="${f.cpf}"/></option>                
                    </c:forEach>
                </select>                                
                <label class="h4">CRB</label>
                <input class="form-control" type="text" name="crb" required>

                <div class="text-center">
                    <button class="btn btn-lg btn-success btn-block" type="submit">Inserir</button>
                </div>
            </form>

        </div>
        
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/bootstrap.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/main.js"></script>                    
    </body>
</html>
