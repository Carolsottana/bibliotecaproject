<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="alertClass" required="true"%>
<%@attribute name="errorMessage" required="true"%>

<c:if test="${not empty sessionScope.error}">
    <div class="alert alert-danger fade in <c:out value="${alertClass}"/>">
        <button class="close" type="button" data-dismiss="alert">&times;</button>
        <c:out value="${errorMessage}"/>
    </div>

    <c:remove var="error" scope="session"/>
</c:if>